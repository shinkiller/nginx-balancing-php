<?php
    session_start();
    $_SESSION['_token'] = isset($_SESSION['_token']) ? $_SESSION['_token'] : uniqid();
?>
<!DOCTYPE html>
<html>
<head>
    <title>[Backend 1] NGINX Load Balancing</h1></title>
    <link rel="icon" href="data:;base64,iVBORw0KGgo=">
</head>
<body>
    <h1>[Backend 1] NGINX Load Balancing - HTTP and TCP Load Balancer - Using PHP 5.6</h1>
    <p><?= $dbStatus ?></p>
    <p>Current token: <?= $_SESSION['_token'] ?></p>
</body>
</html>