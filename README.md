# NGINX Load Balancing - HTTP and TCP Load Balancer

## Usage

#### To install docker
sudo sh docker.sh

#### To start all listed services

sudo sh up.sh

#### To stop all listed services

sudo sh down.sh

### Testing

Access following address:

```
http://localhost:8696
```

"Server 1: http://localhost:8697"
"Server 2: http://localhost:8698"
## References

- [Third-Party Nginx Sticky Module](https://bitbucket.org/nginx-goodies/nginx-sticky-module-ng)
- [NGINX Load Balancing - HTTP and TCP Load Balancer](https://www.nginx.com/resources/admin-guide/load-balancer/#sticky)